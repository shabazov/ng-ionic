import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  form: FormGroup;
  expences: any[] = [];

  constructor(private formBuilder: FormBuilder, public alertController: AlertController) {
    this.form = this.formBuilder.group({
      reason: ['', Validators.required],
      amount: [null, Validators.required]
    })
  }

  // async presentAlert() {
  //   const alert = await this.alertController.create({
  //     header: 'invalid input',
  //     message: 'write a valid data',
  //     buttons: ['okay']
  //   });
  //   await alert.present();
  // };

  reset() {
    this.form.reset();
  }

  submit() {
    if(this.form.invalid) {
      
      return
    };

    const expence = {
      reason: this.form.value.reason,
      amount: this.form.value.amount
    };
    console.log('form value: ', expence);

    this.expences.unshift(expence)

    this.form.reset();

  }

}
